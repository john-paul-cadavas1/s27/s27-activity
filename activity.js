const http = require("http");

// mock items array
let items = [
	
	{
		name: "Iphone X",
		description: "Phone designed and created by Apple",
		price: 30000
	},
	{
		name: "Horizon forbidden West",
		description: "Newest game for the PS4 and PS5",
		price: 40000
	},
	{
		name: "Razer Tiamat",
		description: "Headset from Razer",
		price: 3000
	}
];


http.createServer( (req, res) => {

	if(req.url === '/items' && req.method === "GET"){
		res.writeHead(200, {'Context-Type': 'application/json'});
		res.end(JSON.stringify(items))

	}

	if(req.url === '/items' && req.method === "POST"){
		let requestBody = "";
		req.on('data', function(data){
			requestBody += data;
		})

		req.on('end', function(){
			requestBody = JSON.parse(requestBody);
			items.push(requestBody);

			res.writeHead(200, {'Context-Type': 'application/json'});
			res.end(JSON.stringify(items));
		})

	}

// Stretch goals
	if(req.url === '/items/findItem' && req.method === "POST"){
		req.on('data', function(data){
			itemData = items.find({
				"name": data
			})
		})

		req.on('end', function(){
			itemData = JSON.parse(itemData)
			res.writeHead(200, {'Context-Type': 'application/json'});
			res.end(itemData);
		})
	}

}).listen(8000);

console.log(`Server now accessible at localhost:8000.`);
